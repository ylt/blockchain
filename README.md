Blockchain
----------

This is a blockchain application, there are multiple components of this.

The Chaincode has methods to add businesses, edit them, and to retrieve
them by latlon bounding box.

Another application (node.js) has been implemented to provide a RESTful
API on top of this, which map to PUT/PATCH along with a GET for the search.

For this particular demo, we're importing data from the Food Standards
Agency into the blockchain (http://ratings.food.gov.uk/open-data/en-GB)
for the Bristol area. And a web app has been implemented ontop of the
REST API which allows for a map view which dynamically queries the data.
Markers may be clicked to view more detailed info, and also edit.

### Installation

#### Dependencies
This has been primarily developed and tested on Ubuntu 16.04.
Provided is a deps.sh script which automatically installs the dependencies.
The required dependencies this gets into place are:

 - Docker
 - Docker-Compose
 - Golang
 - libtool libltdl-dev (required for the go code to build)

Which may be manually installed if on another platform.

#### Starting
To start the script, simply execute ./start.sh which hopefully should handle the rest.
The script itself does the following steps:

 - Compiles the chaincode
 - Starts up the hyperledger
 - Installs and instantiates the chaincode
 - Builds the apps 2 docker containers
 - Starts the app
 - Sorts out enrolling the admin and registering the user
 - Starts off FSA data import
 - This is followed by the application logs in follow mode so that operation can be seen.


The docker-compose (located in app dir) is configured to expose itself on port 8000, so you may access the webapp at http://localhost:8000


#### Screenshots/videos

Youtube video is at https://www.youtube.com/watch?v=LAp6z4tcQLo

This shows the overall setup process, along with bits of how it works.


##### Import
[Video](img/import.webm)

##### Map

[Video of map](img/map.webm)

![Map](img/map.png)

![Details](img/map_details.png)

![Edit](img/map_edit.png)
