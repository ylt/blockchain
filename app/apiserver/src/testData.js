const http = require('http');
const fs = require('fs');
const xml2js = require('xml2js');
const get = require('simple-get');
const util = require('util');
const _ = require('lodash');
const geo = require('geo-hash');
const PQueue = require('p-queue');

let API = require('./chain');

let inst = new API();

inst.connect().then(() => {

    //we're using simple-get rather than request or http.get, as it supports gzip/deflate, speeds it up a little
    console.log('downloading food.gov.uk data (may take a short while)');
    get.concat("http://ratings.food.gov.uk/OpenDataFiles/FHRS855en-GB.xml", (error, response, body) => {
        if (error) throw error; // :(

        let parser = new xml2js.Parser();
        parser.parseString(body, (err, result) => {
            if (err) throw err; // :(

            // console.log(JSON.stringify(result));

            let list = result.FHRSEstablishment.EstablishmentCollection[0].EstablishmentDetail;


            let sample = _.filter(list, (v) => {
                    return v.Geocode[0] !== "" && /[0-9]+/.test(v.RatingValue[0])
                });

            if (process.env.DATA_SAMPLE)
                sample = _.sampleSize(sample, parseInt(process.env.DATA_SAMPLE));


            let concurrency = 10;
            if (process.env.DATA_CONCURRENCY)
                concurrency = parseInt(process.env.DATA_CONCURRENCY);

            let queue = new PQueue({concurrency: concurrency});

            let i = 0;
            _.forEach(sample, (est) => {
                queue.add(() => {
                    i++;
                    console.log(est);

                    let hash = geo.encode(est.Geocode[0].Latitude[0], est.Geocode[0].Longitude[0]);

                    //{Geo: args[1], Name: args[2], CompanyNumber: args[3], FoodRating: int(rating)}

                    let com = ['BUS'+i, hash, est.BusinessName[0], est.LocalAuthorityBusinessID[0], est.RatingValue[0]];

                    return inst.invoke('main', 'addBusiness', com);
                });
            });

            queue.onIdle().then(() => {
                console.log('done');
            })

        });
    });

});
//
//
// });