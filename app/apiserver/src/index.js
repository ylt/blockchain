const koa = require('koa');

const logger = require('koa-logger');
const Router = require('koa-router');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');
const app = module.exports = new koa();


let router = new Router();

const Chain = require('./chain');

//get a promise for the chain object when the connect succeeds
let chain = (() => {
    let c = new Chain();
    return c.connect().then(() => {
        return c;
    });
})();
router.get('/search/:geocodes', function (ctx, next) {
    let geocodes = ctx.params.geocodes;
    return Promise.resolve(chain)
        .then((chain) => {
            let codes = geocodes.split(',');

            return chain.query('main', 'queryBusinessByLocation', codes);
        }).then((businesses) => {
            ctx.status = 200;
            ctx.body = businesses;
        });
});

router.put('/business/:id', function (ctx, next) {
    return Promise.resolve(chain)
        .then((chain) => {
            let body = ctx.request.body;
            let args = [
                ctx.params.id,
                body.location || "",
                body.name || "",
                body.companyNumber || "",
                body.foodRating || ""
            ];

            return chain.invoke('main', 'addBusiness', args)
        }).then((response) => {
            ctx.status = 200;
            ctx.body = response;
        }).catch((err) => {
            ctx.status = 500;
            ctx.body = err;
        });
});

router.patch('/business/:id', function (ctx, next) {
    return Promise.resolve(chain)
        .then((chain) => {
            let body = ctx.request.body;
            let args = [
                ctx.params.id,
                body.location || "",
                body.name || "",
                body.companyNumber || "",
                body.foodRating || ""
            ];

            return chain.invoke('main', 'updateBusiness', args)
        }).then((response) => {
            ctx.status = 200;
            ctx.body = response;
        }).catch((err) => {
            ctx.status = 500;
            ctx.body = err;
        });
});

app.use(logger());
app.use(cors());
app.use(bodyParser());

app.use(router.routes());

if (!module.parent) {
    let port = process.env.PORT || 1337;
    app.listen(port);
    console.log('listening on port '+port);
}