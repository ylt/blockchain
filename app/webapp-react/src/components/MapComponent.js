import React, { Component } from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";
import { compose, withProps } from "recompose";
import {changeLocation, fetchCompaniesRegardless, setLocation} from '../actions';

class MyMapComponent extends Component {
    render() {
        return compose(
            withProps({
                googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
                loadingElement: <div style={{ height: `100%` }} />,
                containerElement: <div style={{ height: `400px` }} />,
                mapElement: <div style={{ height: `100%` }} />
            }),
            withScriptjs,
            withGoogleMap)((props) =>
            <GoogleMap
                ref={(ref) => this.map = ref}
                defaultZoom={15}
                defaultCenter={{ lat: 51.4513207, lng: -2.5985083 }}
                onDrag={() => this.onDrag()}
            >
                {props.children}
            </GoogleMap>
        )(this.props);
    }

    shouldComponentUpdate() {
        return false;
    }

    onDrag() {
        let bounds = this.map.getBounds();
        // console.log(bounds);
        this.props.onDrag(bounds);
    }
}

export default MyMapComponent;