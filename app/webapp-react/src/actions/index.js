export const SET_LOCATION = 'SET_LOCATION';
export const REQUEST_COMPANIES = 'REQUEST_COMPANIES';
export const RECEIVE_COMPANIES = 'RECEIVE_COMPANIES';

export const setLocation = (location) => ({
    type: SET_LOCATION,
    location
});


export const requestCompanies = (location) => ({
    type: REQUEST_COMPANIES,
    location
});

export const receiveCompanies = (location, json) => ({
    type: RECEIVE_COMPANIES,
    location,
    companies: json
});

const fetchCompanies = location => dispatch => {
    dispatch(requestCompanies(location))
    return fetch(`http://localhost:3030/search/${location}`)
        .then(response => response.json())
        .then(json => dispatch(receiveCompanies(location, json)))
};

export const fetchCompaniesRegardless = location => (dispatch, getState) => {
    return dispatch(fetchCompanies(location));
};
