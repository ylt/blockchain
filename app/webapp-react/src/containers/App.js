import React, {Component} from 'react';
import logo from '../logo.svg';
import '../App.css';
import MyMapComponent from '../components/MapComponent';
import { connect } from 'react-redux'
import {setLocation} from '../actions';
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps";

class App extends Component {

    constructor() {
        super();

        this.dragged = false;
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <div className="map">
                    <MyMapComponent onDrag={this.onDrag}>
                        {this.dragged &&
                        <Marker position={{lat: 51.4513207, lng: -2.5985083}}/>
                        }
                    </MyMapComponent>
                </div>

            </div>
        );
    }

    onDrag = (location) => {
        this.dragged = true;
        this.props.dispatch(setLocation(location));
    }


}

const mapStateToProps = (state) => {
    const {companiesByAPI, setLocation} = state;
    const {
        isFetching,
        lastUpdated,
        items: items
    } = companiesByAPI[setLocation] || {
        isFetching: true,
        items: []
    };

    return {
        setLocation,
        items,
        isFetching,
        lastUpdated
    };
}

export default connect(mapStateToProps)(App)