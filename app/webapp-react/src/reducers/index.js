import { combineReducers } from 'redux';
import {
    REQUEST_COMPANIES, RECEIVE_COMPANIES, SET_LOCATION
} from '../actions';

const setLocation = (state = 'reactjs', action) => {
    switch (action.type) {
        case SET_LOCATION:
            return action.location;
        default:
            return state;
    }
};

const companies = (state = {
    isFetching: false,
    didInvalidate: false,
    items: []
}, action) => {
    switch(action.type) {
        case SET_LOCATION:

        case REQUEST_COMPANIES:
            return {
                ...state,
                isFetching: true,
                didInvalidate: false
            };
        case RECEIVE_COMPANIES:
            return {
                ...state,
                isFetching: false,
                didInvalidate: false,
                items: action.companies,
                lastUpdated: action.receivedAt
            };
        default:
            return state;
    }
};

const companiesByAPI = (state = { }, action) => {
    switch(action.type) {
        case REQUEST_COMPANIES:
        case RECEIVE_COMPANIES:
            return {
                ...state,
                [action.companies]: companies(state[action.companies], action)
            };
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    companiesByAPI,
    setLocation,
});

export default rootReducer