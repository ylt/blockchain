import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';



class App extends Component {


  render() {
      let that = this;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
          {/*<p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>*/}
          <div className="map">
              <MyMapComponent
                  ref={() => {}}
                  isMarkerShown

                  onDrag={function() { return that.onDrag.apply(this); }} // `this` is referencing the map object
              />
          </div>

      </div>
    );
  }

  onDrag() {
      console.log(this)
  }
}

export default App;
