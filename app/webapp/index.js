const koa = require('koa');

const logger = require('koa-logger');
const Router = require('koa-router');
const proxy = require('koa-proxies');
const static = require('koa-static');

const app = new koa();

let router = new Router();

app.use(logger());

app.use(proxy('/api', {
    target: process.env.APISERVER || 'http://localhost:1337',
    rewrite: path => path.replace(/^\/api/, ''),
    logs: true
}));

app.use(static('public'));


if (!module.parent) {
    let port = process.env.PORT || 8000;
    app.listen(port);
    console.log('listening on port '+port);
}