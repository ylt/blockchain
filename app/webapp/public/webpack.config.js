const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: './js/src/script.js',
    output: {
        filename: 'js/dist/bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/
            }
        ]
    },
    externals: {
        env: 'env'
    }
    // plugins: [
    //     new UglifyJSPlugin({
    //
    //     })
    // ]
};