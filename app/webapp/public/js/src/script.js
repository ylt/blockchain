import * as $ from 'jquery';
import GoogleMapsLoader from 'google-maps';
import geo from 'geo-hash';
import _ from 'lodash';
// import config from '../env'

//let config = window.config;

// $.get

$(() => {

    GoogleMapsLoader.KEY = config.googlekey;
    GoogleMapsLoader.load(() => {
        var uluru = {lat: 51.4513207, lng: -2.5985083};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: uluru,
            styles: [
                {
                    featureType: "poi",
                    stylers: [
                        {visibility: "off"}
                    ]
                }
            ]
        });


        let infowindow = new google.maps.InfoWindow({
            content: document.getElementById('info')
        });

        let editwindow = new google.maps.InfoWindow({
            content: document.getElementById('form')
        });

        //rate limit the bounds event
        let timer = null;
        map.addListener('bounds_changed', () => {
            clearTimeout(timer);
            timer = setTimeout(update, config.debounce);

        });

        let update = () => {
            let bounds = map.getBounds();

            let ne = bounds.getNorthEast();
            let sw = bounds.getSouthWest();

            console.log(ne, sw);

            let hNe = geo.encode(ne.lat(), ne.lng());
            let hSw = geo.encode(sw.lat(), sw.lng());

            fetchPoints(hNe, hSw);

        };

        let points = [];



        function fetchPoints(ne, sw) {
            for (let point of points) {
                point.marker.setMap(null);
            }
            points = [];

            return $.getJSON(config.baseUri + '/search/' + [ne, sw].join(','))
                .then((response, aa) => {
                    for (let record of response) {
                        let pos = geo.decode(record.Record.location);
                        let marker = new google.maps.Marker({
                            position: new google.maps.LatLng(pos.lat, pos.lon),
                            title: record.Record.name,
                            map: map
                        });

                        google.maps.event.addListener(marker, 'click', () => {
                            openInfoWindow(marker);
                        });

                        record.marker = marker;
                        points.push(record);
                    }


                })
                ;
        }

        function openInfoWindow(marker) {
            editwindow.close(); //if it's open, close it

            let details = _.find(points, (p) => p.marker === marker);

            if (!details)
                return;

            let window = $('#info');
            window.find('.record').text(details.Key);
            window.find('.name').text(details.Record.name);
            window.find('.rating').text(details.Record.foodRating);
            window.find('.reference').text(details.Record.companyNumber);

            window.find('button').unbind('click').bind('click', () => {
                openEditWindow(marker);
                return false;
            });

            infowindow.open(map, marker);
        }

        function openEditWindow(marker) {
            infowindow.close();

            let details = _.find(points, (p) => p.marker === marker);

            if (!details)
                return;

            console.log('found', details)

            let window = $('#form');
            window.find('.record').text(details.Key);
            window.find('.name input').val(details.Record.name);
            window.find('.rating input').val(details.Record.foodRating);
            window.find('.reference input').val(details.Record.companyNumber);

            window.find('button').unbind('click').bind('click', () => {
                details.Record.name = window.find('.name input').val();
                details.Record.foodRating = window.find('.rating input').val();
                details.Record.companyNumber = window.find('.reference input').val();

                $.ajax({
                    method: 'PUT',
                    url: config.baseUri+'/business/'+encodeURI(details.Key),
                    data: JSON.stringify(details.Record),
                    contentType: 'application/json',
                    dataType: 'json'
                }).then((response) => {
                    editwindow.close();
                });

                return false;
            });

            editwindow.open(map, marker);
        }

    });




});