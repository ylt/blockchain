package main

import (
"bytes"
"encoding/json"
//"fmt"
"strconv"

"github.com/mmcloughlin/geohash"
"github.com/hyperledger/fabric/core/chaincode/shim"
sc "github.com/hyperledger/fabric/protos/peer"
	"fmt"
	"math"
)


// Define the Smart Contract structure
type SmartContract struct {
}

func (s *SmartContract) Init(APIstub shim.ChaincodeStubInterface) sc.Response {
	return shim.Success(nil)
}


type Business struct {
	Geo string `json:"location"`
	Name string `json:"name"`
	CompanyNumber string `json:"companyNumber"`
	FoodRating int `json:"foodRating"`
}

func (s *SmartContract) Invoke(APIstub shim.ChaincodeStubInterface) sc.Response {
	function, args := APIstub.GetFunctionAndParameters()

	if function == "addBusiness" {
		return s.addBusiness(APIstub, args)
	} else if function == "queryBusiness" {
		return s.queryBusiness(APIstub, args)
	} else if function == "queryBusinessByLocation" {
		return s.queryBusinessByLocation(APIstub, args)
	} else if function == "updateBusiness" {
		return s.updateBusiness(APIstub, args)
	}

	return shim.Error("Invalid Smart Contract function name.")
}

func (s *SmartContract) addBusiness(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 5 {
		return shim.Error("Incorrect number of arguments. Expecting 5")
	}

	rating, err := strconv.ParseInt(args[4], 10, 32);
	if err != nil {
		return shim.Error(err.Error())
	}

	var business = Business{Geo: args[1], Name: args[2], CompanyNumber: args[3], FoodRating: int(rating)}

	businessAsBytes, _ := json.Marshal(business)
	APIstub.PutState(args[0], businessAsBytes)

	return shim.Success(nil)
}


func (s *SmartContract) updateBusiness(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) <= 1 || len(args) > 5 {
		return shim.Error("Incorrect number of arguments. Expecting between 2 and 5 args.")
	}

	businessAsBytes, _ := APIstub.GetState(args[0])
	business := Business{}
	json.Unmarshal(businessAsBytes, business)


	if len(args) > 1 && args[1] != "" {
		business.Geo = args[1]
	}
	if len(args) > 2 && args[2] != "" {
		business.Name = args[2]
	}
	if len(args) > 3 && args[3] != "" {
		business.CompanyNumber = args[3]
	}
	if len(args) > 4 && args[4] != "" {
		rating, err := strconv.ParseInt(args[4], 10, 32);
		if err != nil {
			return shim.Error(err.Error())
		}
		business.FoodRating = int(rating)
	}

	businessAsBytes , _ = json.Marshal(business)
	APIstub.PutState(args[0], businessAsBytes)
	return shim.Success(nil)
}

func (s *SmartContract) queryBusiness(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 {
		return shim.Error("Incorrect number of arguments. Expecting 1")
	}

	businessAsBytes, _ := APIstub.GetState(args[0])
	return shim.Success(businessAsBytes)
}


func (s *SmartContract) queryBusinessByLocation(APIstub shim.ChaincodeStubInterface, args []string) sc.Response {

	if len(args) != 1 && len(args) != 2 {
		return shim.Error("Incorrect number of arguments. Expecting 1-2")
	}

	var query geohash.Box;

	if len(args) == 1 {
		query = geohash.BoundingBox(args[0])
	} else {
		var queryNwLat, queryNwLon = geohash.Decode(args[0])
		var querySeLat, querySeLon = geohash.Decode(args[1])

		query = geohash.Box{
		MinLat: math.Min(querySeLat, queryNwLat),
		MaxLat: math.Max(querySeLat, queryNwLat),
		MinLng: math.Min(querySeLon,queryNwLon),
		MaxLng: math.Max(querySeLon, queryNwLon)}
	}


	fmt.Printf("(%d, %d), (%d, %d)\n", query.MinLat, query.MaxLat, query.MinLng, query.MaxLng)

	resultsIterator, err := APIstub.GetStateByRange("", "")
	if err != nil {
		return shim.Error(err.Error())
	}
	defer resultsIterator.Close()


	// buffer is a JSON array containing QueryResults
	var buffer bytes.Buffer
	buffer.WriteString("[")

	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return shim.Error(err.Error())
		}
		var business Business
		if err := json.Unmarshal(queryResponse.Value, &business); err != nil {
			shim.Error(err.Error())
		}

		var lat,lon = geohash.Decode(business.Geo)
		fmt.Printf("Bus: %d, %d\n", lat, lon)

		if (query.Contains(lat, lon)) {
		//if (lat > query.MinLat && lat < query.MaxLat && lon > query.MinLng && lon < query.MaxLng) {
			buffer.WriteString("{\"Key\":")
			buffer.WriteString("\"")
			buffer.WriteString(queryResponse.Key)
			buffer.WriteString("\"")

			buffer.WriteString(", \"Record\":")
			buffer.WriteString(string(queryResponse.Value))
			buffer.WriteString("},")
		}


	}
	if (buffer.Len() > 2) {
		buffer.Truncate(buffer.Len() - 1)
	}
	buffer.WriteString("]")

	return shim.Success(buffer.Bytes())
}

// The main function is only relevant in unit test mode. Only included here for completeness.
func main() {

	// Create a new Smart Contract
	err := shim.Start(new(SmartContract))
	if err != nil {
		fmt.Printf("Error creating new Smart Contract: %s", err)
	}
}
