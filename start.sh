#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#
# Exit on first error
set -e

# don't rewrite paths for Windows Git Bash users
export MSYS_NO_PATHCONV=1

starttime=$(date +%s)


#sort out the dependencies
export GOPATH=`pwd`/chaincode
(
  rm -rf chaincode/src/github.com
  cd chaincode/src/main;
  go get
  go build
)

(
    cd app
    docker-compose down
)

# launch network; create channel and join peer to channel
(
    cd network
    ./start.sh

    docker-compose -f ./docker-compose.yml up -d cli
)
cli() {
  docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp" cli "$@"
}

export version=$(date +%s);

cli sh -c 'cd main/; go get'
cli peer chaincode install -n main -v $version -p main
cli peer chaincode instantiate -o orderer.example.com:7050 -C mychannel -n main -v $version -c '{"Args":[""]}' -P "OR ('Org1MSP.member','Org2MSP.member')"


printf "\Blockchain setup execution time : $(($(date +%s) - starttime)) secs ...\n\n\n"


#printf "Start by installing required packages run 'npm install'\n"
#printf "Then run 'node enrollAdmin.js', then 'node registerUser'\n\n"
#printf "The 'node invoke.js' will fail until it has been updated with valid arguments\n"
#printf "The 'node query.js' may be run at anytime once the user has been registered\n\n"


(
    cd app
    docker-compose build
    docker-compose up -d
    #docker-compose exec api rm -r src/hfc-key-store || true

    docker-compose exec api node src/enrollAdmin
    docker-compose exec api node src/registerUser
    docker-compose restart api #just in case it's still using older creds

    docker-compose exec api node src/testData
    docker-compose logs -f
)
